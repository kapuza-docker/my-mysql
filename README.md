# Docker / my-mysql
Простой имидж Mysql+ubuntu.
## Получаем репу
```bash
git clone git@gitlab.com:kapuza-docker/my-mysql.git
cd my-mysql
git config user.name "Kaka Puzikova"; git config user.email "gitlab@tost.info.tm"
```
## Использование
```bash
# Сборка имиджа (если он уже склонирован)
docker build -t my-mysql .

# Можно прям из репы (если лень клонировать)
docker build -t my-git git@gitlab.com:kapuza-docker/my-mysql.git

# Создаем контейнер
docker run -d -P --name db my-mysql

# Создаем еще один контейнер, чтоб подключиться к этому
docker run -it --link db:db my-ubuntu /bin/bash
apt install mysql-client
mysql -h db -u admin -pMyAdminPasswd

# Создаем контейнер с новыми паролями
# Дефолтные:
#    MYSQL_ROOT_PASSWD MyRootPasswd
#    MYSQL_ADMIN_PASSWD MyAdminPasswd
docker run -d --env MYSQL_ADMIN_PASSWD=123 --env MYSQL_ROOT_PASSWD=secret -P my-mysql
```
