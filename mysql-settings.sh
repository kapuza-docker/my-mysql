#/bin/bash

# Разрешаем удаленный доступ
sed -i 's/^bind-address.*$/bind-address = 0.0.0.0/' /etc/mysql/mysql.conf.d/mysqld.cnf

# General Log (Пишет все операции - жрет i/o)
#sed -i 's/^#general_log/general_log/' /etc/mysql/mysql.conf.d/mysqld.cnf

# Slow Querie Log
#cat <<EOF >> /etc/mysql/mysql.conf.d/mysqld.cnf
#slow_query_log = 1
#slow_query_log_file = /var/log/mysql/slow-queries.log
#long_query_time = 5
#EOF

# Language Set (Обычно не требуется)
#cat <<EOF >> /etc/mysql/mysql.conf.d/mysqld.cnf
#lc-messages-dir = /usr/share/mysql/english
#EOF

cat <<EOF >> /etc/mysql/mysql.conf.d/mysqld.cnf
# Character Set Server
character-set-server = utf8
EOF

cat <<EOF >> /etc/mysql/mysql.conf.d/mysqld.cnf
# Disable SSL
ssl = false
EOF

# TIMESTAMP
cat <<EOF >> /etc/mysql/mysql.conf.d/mysqld.cnf
explicit_defaults_for_timestamp = 1
EOF
