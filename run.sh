#!/bin/bash

VOLUME_HOME="/var/lib/mysql"


if [[ ! -d $VOLUME_HOME/mysql ]]; then
    echo "=> An empty or uninitialized MySQL volume is detected in $VOLUME_HOME"
    echo "=> Installing MySQL ..."
    mysqld --initialize > /dev/null 2>&1
    echo "=> Done!"  

    mkdir /var/run/mysqld
    chmod 777 /var/run/mysqld

    cat <<EOF > ~/.my.cnf
[client]
user = root
password = ${MYSQL_ROOT_PASSWD}
EOF
    chmod 600 ~/.my.cnf

    cat  <<EOF >  /init.sql
ALTER USER 'root'@'localhost' IDENTIFIED BY '${MYSQL_ROOT_PASSWD}';
CREATE USER 'admin'@'%' IDENTIFIED BY '${MYSQL_ADMIN_PASSWD}';
GRANT ALL PRIVILEGES ON *.* TO 'admin'@'%' WITH GRANT OPTION;
FLUSH PRIVILEGES;
EOF

    /usr/bin/mysqld_safe --init-file=/init.sql > /dev/null 2>&1 &
    sleep 3
    mysql_tzinfo_to_sql /usr/share/zoneinfo | mysql mysql
    sleep 2
    mysqladmin -uroot shutdown

cat <<EOF > /etc/supervisor/conf.d/supervisord-mysqld.conf
[program:mysqld]
command=/usr/bin/mysqld_safe
numprocs=1
autostart=true
autorestart=true
EOF

else
    echo "=> Using an existing volume of MySQL"
fi

exec supervisord -n
