FROM my-ubuntu:latest
MAINTAINER Kaka Puzikova <gitlab@tost.info.tm>

ENV MYSQL_ROOT_PASSWD MyRootPasswd
ENV MYSQL_ADMIN_PASSWD MyAdminPasswd

# Ставим пакеты
ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update && \
  apt-get -y install supervisor mysql-server

# Копируем скрипты
ADD run.sh /run.sh
ADD mysql-settings.sh /mysql-settings.sh
RUN chmod 755 /*.sh
RUN /mysql-settings.sh

# Удаляем предустановленную базу
RUN rm -rf /var/lib/mysql/*

# Добавляем volumes
VOLUME  ["/etc/mysql", "/var/lib/mysql" ]

EXPOSE 3306
CMD ["/run.sh"]
